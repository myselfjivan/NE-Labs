import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

public class ftpKey {
	public static void main(String[] args) {
		try {			
			//System.out.println("Enter the location of downloaded shell script");
			
			String LINUX_CLIENT_BATCH_FILE = "/root/Desktop/ftp/shell_ftp/ftp.sh";
			Process theProcess = Runtime.getRuntime().exec(LINUX_CLIENT_BATCH_FILE);
			File file = new File("/etc/vsftpd/vsftpd.conf");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();

			Enumeration enuKeys = properties.keys();
			System.out.println("This is default configuration file");
			while (enuKeys.hasMoreElements()) {
				String key = (String) enuKeys.nextElement();
				String value = properties.getProperty(key);
				System.out.println(key + ": " + value);
			}
			String connect_from_port_20 = properties.getProperty("connect_from_port_20");
			System.out.println(connect_from_port_20);
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
